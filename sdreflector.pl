#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use Net::DNS::Nameserver;
$|++;

sub reply_handler {
	my ($qname, $qclass, $qtype, $peerhost, $query, $conn) = @_;    
	my ($rcode, @ans, @auth, @add, %headermask);               

	say("Query " . $qname . " received.");
	
	#--------------------------------------------------------
	# parse query
	#--------------------------------------------------------
	$qname = lc($qname); # to lower case
	my (@ips, $ttl, $alias, $qbase);

  my $rx_i  = qr/(\w+(?:\.\w+){0,3})/;	
  my $rx_t  = qr/t(\d{1,6})/;	
  my $rx_a  = qr/((?:\w|-)+)/;
  my $rx_q  = qr/((?:\w+\.)*\w+)/; 
	if($qname =~ /^${rx_i}\.${rx_t}\.${rx_a}\.${rx_q}$/) {
    @ips = split('\.', $1);
		($ttl, $alias, $qbase) = ($2,$3,$4);
		$rcode  = "NOERROR";
	}
	else{
		$rcode = "NXDOMAIN";
	  return ($rcode, \@ans, \@auth, \@add, \%headermask);
	}
	
  #--------------------------------------------------------
  # assemble answer
  #--------------------------------------------------------
  $headermask{aa} = 1; # set authoritative answer flag
  my $alias_domain = $alias . '.' . $qbase;
  my $ns_num = 1;
  foreach my $ip (@ips){
    my $ns_domain    = 'ns' . $ns_num++ . '.' . $alias_domain;
    #convert from hex to dotted notation
    $ip = join '.', unpack "C*", pack "H*", $ip; 
    my $rr_ns = new Net::DNS::RR(name => $alias_domain,
      ttl => $ttl,
      class => "IN",
      type => "NS",
      nsdname => $ns_domain);
    push @auth, $rr_ns;
    my $rr_a = new Net::DNS::RR(name => $rr_ns->nsdname,
      ttl => $ttl,
      class => "IN",
      type => "A",
      address => $ip);
    push @add, $rr_a;
  }

	# return answer
	return ($rcode, \@ans, \@auth, \@add, \%headermask);
}

# create nameserver object
my $ns = new Net::DNS::Nameserver(
  LocalAddr    => "51.254.124.217",
  LocalPort    => 53,
  ReplyHandler => \&reply_handler,
  # Verbose	     => 0
) || die "couldn't create nameserver object\n";

# start nameserver main loop
$ns->main_loop;

